package com.supakit.app03
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.supakit.app03.adapter.ItemAdapter
import com.supakit.app03.databinding.ActivityMainBinding
import com.supakit.app03.model.Oil

class MainActivity : AppCompatActivity() {
    private lateinit var binding : ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val data = listOf<Oil>(
            Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ E20", 36.84),
            Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 91", 37.68),
            Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 95", 37.95),
            Oil("เชลล์ วี-เพาเวอร์ แก๊สโซฮอล์ 95", 45.44),
            Oil("เชลล์ ดีเซล B20", 36.34),
            Oil("เชลล์ ฟิวเซฟ ดีเซล", 36.34),
            Oil("เชลล์ ฟิวเซฟ ดีเซล B7", 36.34),
            Oil("เชลล์ วี-เพาเวอร์ ดีเซล", 36.34),
            Oil("เชลล์ วี-เพาเวอร์ ดีเซล B7", 47.06)
        )
        val recyclerView = binding.recycleView
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = ItemAdapter(data,applicationContext)
    }

}

