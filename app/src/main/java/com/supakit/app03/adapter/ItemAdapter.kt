package com.supakit.app03.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.supakit.app03.R
import com.supakit.app03.model.Oil

class ItemAdapter(val data:List<Oil>, val context: Context) : RecyclerView.Adapter<ItemAdapter.ViewHolder>() {
    inner class ViewHolder(private val itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title = itemView.findViewById<TextView>(R.id.listItem2)
        val price = itemView.findViewById<TextView>(R.id.listItem1)
        val item = itemView.findViewById<LinearLayout>(R.id.item)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val adapterLayout = LayoutInflater.from(parent.context).inflate(R.layout.item_list,parent,false)
        return ViewHolder(adapterLayout)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var item = data[position]
        holder.title.text = item.title
        holder.price.text = item.price.toString()
        holder.item.setOnClickListener{
            Toast.makeText(context,
                "${item.title} ${item.price.toString()} " , Toast.LENGTH_LONG).show()
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

}